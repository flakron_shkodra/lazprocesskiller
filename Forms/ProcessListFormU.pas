unit ProcessListFormU;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, strutils;

type

  { TProcessListForm }

  TProcessListForm = class(TForm)
    btnOk: TButton;
    btnCancel: TButton;
    txtSearchByName: TEdit;
    lstProcesses: TListBox;
    procedure FormShow(Sender: TObject);
    procedure txtSearchByNameChange(Sender: TObject);
    procedure txtSearchByNameEnter(Sender: TObject);
    procedure txtSearchByNameKeyPress(Sender: TObject; var Key: char);
  private
    { private declarations }
    FLastSearchWord: string;
    function FindPosition(alist: TStrings; aText: string;
      aStartIndex: integer = 0): integer;
    procedure SearchProcessByName;
  public
    { public declarations }
  end;

var
  ProcessListForm: TProcessListForm;

implementation

uses ProcessLister;

{$R *.lfm}

{ TProcessListForm }



procedure TProcessListForm.FormShow(Sender: TObject);
var
  lst: TStringList;
begin
  with TProcessLister.Create do
  begin
    try
      lst := GetRunningProcesses;
      lstProcesses.Items.AddStrings(lst);
    finally
      lst.Free;
    end;
    Free;
  end;
  txtSearchByName.Font.Color := clSilver;
  txtSearchByName.Text := 'Search';
end;

procedure TProcessListForm.txtSearchByNameChange(Sender: TObject);
begin
  SearchProcessByName;
end;

procedure TProcessListForm.txtSearchByNameEnter(Sender: TObject);
begin
  txtSearchByName.Font.Color := clDefault;
  txtSearchByName.Clear;
end;

procedure TProcessListForm.txtSearchByNameKeyPress(Sender: TObject; var Key: char);
begin
  if Key = #13 then
    SearchProcessByName;
end;

function TProcessListForm.FindPosition(alist: TStrings; aText: string;
  aStartIndex: integer): integer;
var
  I: integer;
begin
  if aStartIndex + 1 < aList.Count then
    Inc(aStartIndex, 1);

  for I := aStartIndex to aList.Count - 1 do
  begin
    if AnsiStartsText(aText, aList[I]) then
    begin
      Result := I;
      Break;
    end;
  end;
end;

procedure TProcessListForm.SearchProcessByName;
var
  sPos: integer;
begin
  if Trim(txtSearchByName.Text) = EmptyStr then
    exit;

  if (FLastSearchWord = txtSearchByName.Text) then
    sPos := lstProcesses.ItemIndex
  else
    sPos := 0;
  lstProcesses.ItemIndex := FindPosition(lstProcesses.Items, txtSearchByName.Text, sPos);
  FLastSearchWord := txtSearchByName.Text;

end;

end.
