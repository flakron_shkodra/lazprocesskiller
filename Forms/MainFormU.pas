unit MainFormU;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, Menus, Spin, ExtCtrls, PopupNotifier, ProcessLister, registry, IniFiles,
  Windows;

type

  { TMainForm }

  TMainForm = class(TForm)
    btnListProcesses: TSpeedButton;
    btnOk: TBitBtn;
    btnClose: TBitBtn;
    chkAutostart: TCheckBox;
    chkShowNotification: TCheckBox;
    grpOptions: TGroupBox;
    lblKillInterval: TLabel;
    lbLSecond: TLabel;
    mitExitApp: TMenuItem;
    mitSep1: TMenuItem;
    mitApp: TMenuItem;
    mitAbout: TMenuItem;
    mitExit: TMenuItem;
    mitHelp: TMenuItem;
    mitFile: TMenuItem;
    mmMainMenu: TMainMenu;
    mitDelete: TMenuItem;
    popProcess: TPopupMenu;
    popApplication: TPopupMenu;
    sedKillInterval: TSpinEdit;
    tmrCheckNotifier: TTimer;
    tmrKillTimer: TTimer;
    TrayIcon1: TTrayIcon;
    txtProcessName: TEdit;
    grpEnemyProcess: TGroupBox;
    Label1: TLabel;
    lstKillProcesses: TListBox;
    btnAdd: TSpeedButton;
    procedure btnAddClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnListProcessesClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure chkAutostartChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure mitAboutClick(Sender: TObject);
    procedure mitAppClick(Sender: TObject);
    procedure mitDeleteClick(Sender: TObject);
    procedure mitExitAppClick(Sender: TObject);
    procedure mitExitClick(Sender: TObject);
    procedure mitHelpClick(Sender: TObject);
    procedure sedKillIntervalChange(Sender: TObject);
    procedure tmrCheckNotifierTimer(Sender: TObject);
    procedure tmrKillTimerTimer(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
    procedure TrayIcon1DblClick(Sender: TObject);
  private
    procedure AddProcessToKill(ProcessName: string);
    procedure CloseApplication;
    procedure ReadAppSettings;
    procedure SaveAppSettings;

    procedure RemoveSystrayDeadIcons;
  public
    { public declarations }
  end;

procedure GetRunningProcesses(var sList: TStringList);

const
  AppName = 'Process Killer';
  AppVersion = '1.4';

var
  MainForm: TMainForm;
  PlFilePath: string;
  ProcLister: TProcessLister;
  notifier: TPopupNotifier;

implementation

uses ProcessListFormU;

{$R *.lfm}

{ TMainForm }

procedure GetRunningProcesses(var sList: TStringList);
begin

end;

procedure TMainForm.FormCreate(Sender: TObject);
begin

  ProcLister := TProcessLister.Create;
  PlFilePath := Application.ExeName + '.pl';

  if FileExists(PlFilePath) then
  begin
    lstKillProcesses.Items.LoadFromFile(PlFilePath);
  end;

  ReadAppSettings;

  TrayIcon1.Visible := True;
  notifier := TPopupNotifier.Create(Self);
  with notifier do
  begin
    ;
    Icon.Assign(Application.Icon);
    Title := 'Process Killer';
    vNotifierForm.Width := 250;
    vNotifierForm.Height := 60;
    vNotifierForm.AutoHide := True;
    vNotifierForm.HideInterval := 2;
    vNotifierForm.AlphaBlend := True;
    vNotifierForm.AlphaBlendValue := 200;
  end;

  Caption := AppName + ' ' + AppVersion;

end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  ProcLister.Free;
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
  if WindowState = wsMinimized then
  begin
    Hide;
  end;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  if (ParamStr(1) = 's') and (Self.Tag = 0) then
  begin
    btnOkClick(Sender);
  end;
end;

procedure TMainForm.mitAboutClick(Sender: TObject);
begin
  ShowMessage('Process Killer ' + AppVersion + ' ' + LineEnding +
    '===========================' + LineEnding +
    ' kills processes on list based on interval given' + LineEnding +
    '============================' + LineEnding + 'Flakron Shkodra 2011');
end;

procedure TMainForm.mitAppClick(Sender: TObject);
begin
  Self.Tag := 1;
  Self.Show;
  tmrKillTimer.Enabled := False;
end;

procedure TMainForm.mitDeleteClick(Sender: TObject);
begin
  if lstKillProcesses.ItemIndex > 0 then
  begin
    lstKillProcesses.Items.Delete(lstKillProcesses.ItemIndex);
    lstKillProcesses.Items.SaveToFile(PlFilePath);
  end;
end;

procedure TMainForm.mitExitAppClick(Sender: TObject);
begin
  CloseApplication;
end;

procedure TMainForm.mitExitClick(Sender: TObject);
begin
  CloseApplication;
end;

procedure TMainForm.mitHelpClick(Sender: TObject);
begin

end;

procedure TMainForm.sedKillIntervalChange(Sender: TObject);
begin
  tmrKillTimer.Enabled := False;
  tmrKillTimer.Interval := sedKillInterval.Value * 1000;
end;

procedure TMainForm.tmrCheckNotifierTimer(Sender: TObject);
begin
  tmrCheckNotifier.Enabled := False;
  if notifier.Visible then
    notifier.Hide;
end;

procedure TMainForm.tmrKillTimerTimer(Sender: TObject);
var
  I: integer;
begin
  tmrKillTimer.Enabled := False;
  for I := 0 to lstKillProcesses.Count - 1 do
  begin
    if (ProcLister.KillProcess(lstKillProcesses.Items[I])) then
    begin
      if chkShowNotification.Checked then
      begin
        notifier.Text := 'Process killed: [' + lstKillProcesses.Items[I] + ']';
        notifier.ShowAtPos(Screen.Width, Screen.Height);
        tmrCheckNotifier.Enabled := True;
      end;
      RemoveSystrayDeadIcons;
    end;

  end;
  tmrKillTimer.Enabled := True;
end;

procedure TMainForm.TrayIcon1Click(Sender: TObject);
begin
  TrayIcon1.BalloonTitle := 'Info';
  TrayIcon1.BalloonHint := 'Process Killer (RightClick for menu)';
  TrayIcon1.BalloonFlags := bfInfo;
  TrayIcon1.ShowBalloonHint;
end;

procedure TMainForm.TrayIcon1DblClick(Sender: TObject);
begin
  TrayIcon1Click(Sender);
end;

procedure TMainForm.AddProcessToKill(ProcessName: string);
begin
  lstKillProcesses.Items.Add(ProcessName);
  lstKillProcesses.Items.SaveToFile(PlFilePath);
  txtProcessName.Clear;
end;

procedure TMainForm.CloseApplication;
begin
  SaveAppSettings;
  Application.Terminate;
end;

procedure TMainForm.ReadAppSettings;
begin
  with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do
  begin
    chkAutostart.Checked := ReadBool('Settings', 'Autostart', True);
    chkShowNotification.Checked := ReadBool('Settings', 'Notification', True);
    sedKillInterval.Value := ReadInteger('Settings', 'KillInterval', 2);
    Free;
  end;
end;

procedure TMainForm.SaveAppSettings;
begin
  with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do
  begin
    WriteBool('Settings', 'Autostart', chkAutostart.Checked);
    WriteBool('Settings', 'Notification', chkShowNotification.Checked);
    WriteInteger('Settings', 'KillInterval', sedKillInterval.Value);
    Free;
  end;
end;

procedure TMainForm.RemoveSystrayDeadIcons;
var
  TrayWindow: HWnd;
  WindowRect: TRect;
  CursorPos: TPoint;
  Row: integer;
  Col: integer;
begin
  { Get tray window handle and bounding rectangle }
  TrayWindow := FindWindowEx(FindWindow('Shell_TrayWnd', nil), 0, 'TrayNotifyWnd', nil);
  if not GetWindowRect(TrayWindow, WindowRect) then
    Exit;
  { Save current mouse position }
  GetCursorPos(CursorPos);
  { Sweep the mouse cursor over each icon in the tray }
  Row := WindowRect.Top + 8;  // 8 is half the standard Tray Icon height
  Col := WindowRect.left;
  while Col < WindowRect.Right do
  begin
    SetCursorPos(Col, Row);
    Sleep(1);
    Inc(Col, 8);  // 8 is half the standard Tray Icon width
  end;
  { Restore mouse position }
  SetCursorPos(CursorPos.X, CursorPos.Y);
  { Redraw tray window (to fix bug in multi-line tray area) }
  RedrawWindow(TrayWindow, nil, 0, RDW_INVALIDATE or RDW_ERASE or RDW_UPDATENOW);
end;


procedure TMainForm.btnAddClick(Sender: TObject);
begin
  if Trim(txtProcessName.Text) <> EmptyStr then
  begin
    AddProcessToKill(txtProcessName.Text);
  end;
end;

procedure TMainForm.btnCloseClick(Sender: TObject);
begin

end;

procedure TMainForm.btnListProcessesClick(Sender: TObject);
begin

  with ProcessListForm do
    if ShowModal = mrOk then
    begin
      AddProcessToKill(ChangeFileExt(lstProcesses.Items[lstProcesses.ItemIndex],''));
    end;
end;

procedure TMainForm.btnOkClick(Sender: TObject);
begin
  if lstKillProcesses.Items.Count > 0 then
  begin
    tmrKillTimer.Enabled := True;
    Hide;
    TrayIcon1.BalloonFlags := bfInfo;
    TrayIcon1.BalloonTitle := AppName + ' ' + AppVersion;
    TrayIcon1.BalloonHint := 'Application running in background' + LineEnding;
    TrayIcon1.ShowBalloonHint;
  end;
end;

procedure TMainForm.chkAutostartChange(Sender: TObject);
var
  r: TRegistry;
begin
  r := TRegistry.Create;
  try
    r.RootKey := HKEY_LOCAL_MACHINE;

    if r.OpenKey('Software\Microsoft\Windows\CurrentVersion\Run', False) then
    begin

      if chkAutostart.Checked then
      begin
        r.WriteString(AppName, Application.ExeName + ' s');
      end
      else
      begin
        r.DeleteValue(AppName);
      end;

    end;

  finally
    r.Free;
  end;
end;

procedure TMainForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  SaveAppSettings;
end;

end.
