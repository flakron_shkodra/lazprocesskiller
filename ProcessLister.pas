unit ProcessLister;

{$mode objfpc}{$H+}

interface

uses
  Classes,SysUtils,windows,JwaTlHelp32,process;

type

  { TProcessLister }

  TProcessLister = class
  private
    proc:TProcess;
  public
    constructor Create;
    function GetProcessHandle(ProcessName:string):THandle;
    function GetRunningProcesses:TStringList;
    function KillProcess(ExeFileName:string):Boolean;
    destructor Destroy;override;
  end;

implementation

{ TProcessLister }

constructor TProcessLister.Create;
begin
  inherited Create;
  proc := TProcess.Create(nil);
  proc.ApplicationName:='tskill.exe';
end;

function TProcessLister.GetProcessHandle(ProcessName: string): THandle;
var
  pa: TProcessEntry32;
  RetVal: THandle;
  sList : TStringList;
  _procName:string;
  GivenProcName:string;
begin
  Result := 0;
  GivenProcName:=lowercase(ProcessName);

  if ExtractFileExt(GivenProcName)='' then
  begin
    GivenProcName:= ChangeFileExt(GivenProcName,'.exe');
  end;

  RetVal := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  pa.dwSize := sizeof(pa);

  if Process32First(RetVal, pa) then
  begin
    _procName:= lowercase(pa.szExeFile);
    if (_procName=GivenProcName) then
    begin
      Result := pa.th32ProcessID;
    end else
    while Process32Next(RetVal, pa)  do
    begin
      _procName := lowercase(pa.szExeFile);
      if _procName = GivenProcName then
      begin
        Result := pa.th32ProcessID;
        Break;
      end;
    end;
  end;

end;

function TProcessLister.GetRunningProcesses: TStringList;
var
  pa: TProcessEntry32;
  RetVal: THandle;
  sList : TStringList;
begin
  RetVal := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  pa.dwSize := sizeof(pa);
  //Create string list
  sList := TStringList.Create;
  //Get first process
  if Process32First(RetVal, pa) then
    //Add process name to string list
    sList.Add(pa.szExeFile);
  begin
    //While we have process handle
    while Process32Next(RetVal, pa) do
    begin
      sList.Add(pa.szExeFile);
    end;
  end;
  //Assign to listbox or what ever you want to do
  Result := sList;
end;

function TProcessLister.KillProcess(ExeFileName: string): Boolean;
var
  h,rv:THANDLE;
  sysdir:array [0..MaxPathLen] of char;
begin
  rv := 0;
  h := GetProcessHandle(ExeFileName);
  if h<>0 then
  begin
   Result := TerminateProcess(
            OpenProcess(PROCESS_TERMINATE, BOOL(0),h),
            0
          );

   //burte force
   if not Result then
   begin
      GetSystemDirectory(@sysdir,Length(sysdir));
      proc.ApplicationName:=sysdir+'\taskkill.exe';
      proc.CommandLine:='taskkill.exe /F /IM '+ChangeFileExt(ExeFileName,'.exe')+' /T';
      proc.ShowWindow:=swoHIDE;
      try
        proc.Execute;
        Result:=True;
      except
        Result := False;
      end;

   end;

  end;
end;

destructor TProcessLister.Destroy;
begin
  proc.Free;
  inherited Destroy;
end;


end.

